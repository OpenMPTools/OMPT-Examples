#include <stdio.h>
#include <omp.h>
#include <math.h>

#define SIZE 1000000

int main()
{
  #pragma omp task
  {
    printf("task\n");
    #pragma omp task
    {
      double sum=0;
      for (int i=0; i < SIZE; i++)
        sum+=sin(.1*i)*cos(.1*i);
      printf("nested task: %f\n", sum);
    }
  }

  #pragma omp parallel num_threads(2)
  {
    #pragma omp single
    #pragma omp task
    {
      double sum=0;
      #pragma omp task
      {
        double sum=0;
        for (int i=0; i < SIZE; i++)
          sum+=sin(.1*i)*cos(.1*i);
        printf("nested task in parallel: %f\n", sum);
      }
      for (int i=0; i < SIZE; i++)
        sum+=sin(.1*i)*cos(.1*i);
      printf("task in parallel: %f\n", sum);
    }
  }

  return 0;
}
